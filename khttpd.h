/*
 *  khttpd.h - Linux kernel module for kHTTPd Daemon
 *
 *  Copyright (C) 1999 Arjan van de Ven
 *  Copyright (C) 2018 Kieran Grant <kierangrant@linux.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/kconfig.h>

#ifndef _KHTTPD_H_
#define _KHTTPD_H_

#ifndef CONFIG_KHTTPD_DEFAULT_PORT
#define CONFIG_KHTTPD_DEFAULT_PORT	8080
#endif
#ifndef CONFIG_KHTTPD_DEFAULT_WWWROOT
#define CONFIG_KHTTPD_DEFAULT_WWWROOT	"/var/www"
#endif
#ifndef CONFIG_KHTTPD_DEFAULT_MAXCONN
#define CONFIG_KHTTPD_DEFAULT_MAXCONN 1000
#endif
#ifndef CONFIG_KHTTPD_DEFAULT_THREADS
#define CONFIG_KHTTPD_DEFAULT_THREADS 4
#endif

#define MAX_SMLBUF_SIZE	16
#define MAX_BUF_SIZE	256

#define MOD_PREFIX	"kHTTPd: "
#define mod_print(LEVEL, ...)	printk(LEVEL MOD_PREFIX __VA_ARGS__)

/* ProcFS control */
int khttpd_setup_procfs(void);
void khttpd_teardown_procfs(void);

int khttpd_open(struct inode *, struct file*);
int khttpd_close(struct inode *, struct file*);
ssize_t khttpd_write(struct file *, const char __user *, size_t, loff_t *);
ssize_t khttpd_read(struct file *, char __user *, size_t, loff_t *);

/* Server Control */
int khttpd_start_server(void);
int khttpd_stop_server(void);
int khttpd_server_alive(void);
void khttpd_inhibit_server(void);

struct khttp_request;

struct khttpd_request
{
  struct khttpd_request *next;
  struct socket *sock;
  unsigned long visitor_number;
};

struct khttpd_request *khttpd_request_get(void);
struct khttpd_request *khttpd_request_new(struct socket *);
void khttpd_request_release(struct khttpd_request *);

/* Server Configuration */
int khttpd_server_get_port(void);
int khttpd_server_set_port(int);
char *khttpd_server_get_wwwroot(void);
int khttpd_server_set_wwwroot(char *);
int khttpd_server_get_maxcon(void);
int khttpd_server_set_maxcon(int);
int khttpd_server_get_nrcons(void);
unsigned long khttpd_server_get_total_visitors(void);
int khttpd_server_get_nr_threads(void);
int khttpd_server_set_nr_threads(int);

/* Worker Thread */
int khttpd_worker(void *);

#endif
