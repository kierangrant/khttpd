/*
 *  init.c - Linux kernel module for kHTTPd Daemon
 *
 *  Copyright (C) 2018 Kieran Grant <kierangrant@linux.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/reboot.h>
#include "khttpd.h"

#define INIT_MSG "module loaded\n"
#define EXIT_MSG "module unloaded\n"

static int khttpd_shutdown(struct notifier_block *, unsigned long, void *);

static struct notifier_block khttpd_shutdown_nb = {
   .notifier_call = khttpd_shutdown,
};

static int __init khttpd_load(void)
{
  if (!khttpd_setup_procfs()) {
    mod_print(KERN_WARNING, "khttpd_load: Failed to setup proc filesystem entries\n");
    return -1;
  }

  if (!khttpd_server_set_wwwroot(CONFIG_KHTTPD_DEFAULT_WWWROOT))
    return -1;

  register_reboot_notifier(&khttpd_shutdown_nb);

  mod_print(KERN_NOTICE, INIT_MSG);
  return 0;
}

static void __exit khttpd_unload(void)
{
  khttpd_teardown_procfs();
  /* My logic for stopping server may be flakey! */
  WARN(unlikely(khttpd_server_alive()), MOD_PREFIX "Module unloaded whilst server still running!");
  unregister_reboot_notifier(&khttpd_shutdown_nb);
  mod_print(KERN_NOTICE, EXIT_MSG);
}

/* We must shutdown */
static int khttpd_shutdown(struct notifier_block *unused1, unsigned long unused2, void *unused3)
{
  khttpd_inhibit_server();
  khttpd_stop_server();

  return 0;
}

module_init(khttpd_load);
module_exit(khttpd_unload);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Kieran Grant");
