/*
 *  worker.c - Linux kernel module for kHTTPd Daemon
 *
 *  Copyright (C) 2018 Kieran Grant <kierangrant@linux.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kthread.h>
#include <linux/net.h>
#include <linux/nsproxy.h>
#include <net/sock.h>
#include <uapi/linux/in.h>
#include <linux/delay.h>
#include <linux/mutex.h>
#include "khttpd.h"

static char *header = "\
HTTP/1.0 200 Success\r\n\
Content-Length: %d\r\n\
Content-Type: text/html\r\n\r\n";
static char *message = "\
<html>\n\
  <head>\n\
    <title>Hello from the Linux Kernel - kHTTPd</title>\n\
  </head>\n\
  <body>\n\
    <div>You are visitor: %lu. There are currently %lu visitors online.</div>\n\
  </body>\n\
</html>";

int khttpd_worker(void *useless)
{
  while (!kthread_should_stop()) {
    struct msghdr *msg;
    struct kvec *iovec;
    struct khttpd_request *req;
    char *buf;
    int error;
    int worker_sleep = 1;

    req = khttpd_request_get();
    if (!req)
      goto _nofree;

    msg = kzalloc(sizeof(struct msghdr), GFP_KERNEL);
    if (!msg)
      goto _release_req;

    iovec = kcalloc(2, sizeof(struct iovec), GFP_KERNEL);
    if (!iovec)
      goto _free_msg;

    buf = kmalloc(1024, GFP_KERNEL);
    if (!buf)
      goto _free_iovec;

    msg->msg_name	= NULL;
    msg->msg_control	= NULL;
    iovec->iov_base	= buf;
    iovec->iov_len	= 1024;

    /* read data. currently doesn't actual handle EWOULDBLOCK or EAGAIN! */
    error = kernel_recvmsg(req->sock, msg, iovec, 1, iovec->iov_len, 0);
    if (error < 0) {
      mod_print(KERN_WARNING, "khttpd_server: Problem reading socket\n");
      goto _free_buf;
    }

    memset(msg, 0, sizeof(struct msghdr));
    msg->msg_name	= NULL;
    msg->msg_control	= NULL;
    memset(iovec, 0, sizeof(struct kvec));

    /* Build message */
    error = snprintf(buf, 512, message, req->visitor_number,
			 khttpd_server_get_nrcons());
    if (error < 0)
      goto _free_buf;
    if (error == 512)
      --error;
    iovec[1].iov_base = buf;
    iovec[1].iov_len  = error;

    error = snprintf(&buf[512], 512, header, iovec[1].iov_len);
    if (error < 0)
      goto _free_buf;
    if (error == 512)
      --error;
    iovec[0].iov_base = &buf[512];
    iovec[0].iov_len  = error;

    /* send response */
    error = kernel_sendmsg(req->sock, msg, iovec, 2,
			   iovec[0].iov_len + iovec[1].iov_len);
    worker_sleep = 0;
    if (error < 0) {
      mod_print(KERN_WARNING, "khttpd_server: Failed to send response\n");
      worker_sleep = 1;
    }

  _free_buf:
    kfree(buf);
  _free_iovec:
    kfree(iovec);
  _free_msg:
    kfree(msg);
  _release_req:
    khttpd_request_release(req);
  _nofree:
    if (worker_sleep)
      usleep_range(1000,10000);
  }

  return 0;
}
