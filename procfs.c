/*
 *  protcfs.c - Linux kernel module for kHTTPd Daemon
 *
 *  Copyright (C) 1999 Arjan van de Ven
 *  Copyright (C) 2018 Kieran Grant <kierangrant@linux.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>
#include <linux/cred.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include "khttpd.h"

#define KHTTPD_PARENT	"driver/khttpd"
#define KHTTPD_CONTROL	"status"
#define KHTTPD_PORT	"port"
#define KHTTPD_ROOT	"wwwroot"
#define KHTTPD_MAXCON	"max_nr_of_connections"
#define KHTTPD_NRCONS	"nr_of_connections"
#define KHTTPD_VISITORS	"total_visitors"
#define KHTTPD_THREADS	"nr_of_threads"

enum KHTTPD_PROC_ENTIRIES
  {
   _KHTTPD_CONTROL = 1,
   _KHTTPD_PORT,
   _KHTTPD_WWWROOT,
   _KHTTPD_MAXCON,
   _KHTTPD_NRCONS,
   _KHTTPD_VISITORS,
   _KHTTPD_THREADS,
  };

/* PROC File System entries */
static struct proc_dir_entry *khttpd_dir;
static struct proc_dir_entry *khttpd_status;
static struct proc_dir_entry *khttpd_port;
static struct proc_dir_entry *khttpd_wwwroot;
static struct proc_dir_entry *khttpd_maxcon;
static struct proc_dir_entry *khttpd_nrcons;
static struct proc_dir_entry *khttpd_visitors;
static struct proc_dir_entry *khttpd_threads;

static struct file_operations myfops =
  {
   .owner	= THIS_MODULE,
   .open	= khttpd_open,
   .release	= khttpd_close,
   .write	= khttpd_write,
   .read	= khttpd_read
  };

static struct file_operations myfops_ro =
  {
   .owner	= THIS_MODULE,
   .open	= khttpd_open,
   .release	= khttpd_close,
   .read	= khttpd_read
  };

/* khttpd_{setup,teardown}_procfs are called only in module init and exit functions */

int __init khttpd_setup_procfs(void)
{
  khttpd_dir = proc_mkdir(KHTTPD_PARENT, NULL);
  if (IS_ERR(khttpd_dir)) {
    mod_print(KERN_WARNING, "Failed to create /proc/" KHTTPD_PARENT "\n");
    goto _unwind;
    return 0;
  }

  /* Using ERR_PTR is a bit of an abuse here */
  khttpd_status = proc_create_data(KHTTPD_CONTROL, 0644, khttpd_dir, &myfops, ERR_PTR(_KHTTPD_CONTROL));
  if (IS_ERR(khttpd_status)) {
    mod_print(KERN_WARNING, "Failed to create /proc/" KHTTPD_PARENT "/" KHTTPD_CONTROL "\n");
    goto _unwind_dir;
  }

  khttpd_port = proc_create_data(KHTTPD_PORT, 0644, khttpd_dir, &myfops, ERR_PTR(_KHTTPD_PORT));
  if (IS_ERR(khttpd_port)) {
    mod_print(KERN_WARNING, "Failed to create /proc" KHTTPD_PARENT "/" KHTTPD_PORT "\n");
    goto _unwind_status;
  }

  khttpd_wwwroot = proc_create_data(KHTTPD_ROOT, 0644, khttpd_dir, &myfops, ERR_PTR(_KHTTPD_WWWROOT));
  if (IS_ERR(khttpd_wwwroot)) {
    mod_print(KERN_WARNING, "Failed to create /proc" KHTTPD_PARENT "/" KHTTPD_ROOT "\n");
    goto _unwind_port;
  }

  khttpd_maxcon = proc_create_data(KHTTPD_MAXCON, 0644, khttpd_dir, &myfops, ERR_PTR(_KHTTPD_MAXCON));
  if (IS_ERR(khttpd_maxcon)) {
    mod_print(KERN_WARNING, "Failed to create /proc" KHTTPD_PARENT "/" KHTTPD_MAXCON "\n");
    goto _unwind_root;
  }

  khttpd_nrcons = proc_create_data(KHTTPD_NRCONS, 0444, khttpd_dir, &myfops_ro, ERR_PTR(_KHTTPD_NRCONS));
  if (IS_ERR(khttpd_nrcons)) {
    mod_print(KERN_WARNING, "Failed to create /proc" KHTTPD_PARENT "/" KHTTPD_NRCONS "\n");
    goto _unwind_maxcon;
  }

  khttpd_visitors = proc_create_data(KHTTPD_VISITORS, 0444, khttpd_dir, &myfops_ro, ERR_PTR(_KHTTPD_VISITORS));
  if (IS_ERR(khttpd_visitors)) {
    mod_print(KERN_WARNING, "Failed to create /proc" KHTTPD_PARENT "/" KHTTPD_VISITORS "\n");
    goto _unwind_nrcons;
  }

  khttpd_threads = proc_create_data(KHTTPD_THREADS, 0644, khttpd_dir, &myfops, ERR_PTR(_KHTTPD_THREADS));
  if (IS_ERR(khttpd_threads)) {
    mod_print(KERN_WARNING, "Failed to create /proc" KHTTPD_PARENT "/" KHTTPD_THREADS "\n");
    goto _unwind_visitors;
  }

  /* success */
  return 1;

 _unwind_visitors:
  proc_remove(khttpd_visitors);
 _unwind_nrcons:
  proc_remove(khttpd_nrcons);
 _unwind_maxcon:
  proc_remove(khttpd_maxcon);
 _unwind_root:
  proc_remove(khttpd_wwwroot);
 _unwind_port:
  proc_remove(khttpd_port);
 _unwind_status:
  proc_remove(khttpd_status);
 _unwind_dir:
  proc_remove(khttpd_dir);
 _unwind:
  return 0;
}

void __exit khttpd_teardown_procfs(void)
{
  proc_remove(khttpd_threads);
  proc_remove(khttpd_visitors);
  proc_remove(khttpd_nrcons);
  proc_remove(khttpd_maxcon);
  proc_remove(khttpd_wwwroot);
  proc_remove(khttpd_port);
  proc_remove(khttpd_status);
  proc_remove(khttpd_dir);
}

/* Try to read in the status from user.
   0 - Stop
   1 - Start
 */
static int try_process_status(char *data)
{
  int ret;
  long val;

  ret = kstrtol(data, 10, &val);
  if (ret < 0) {
    return -EINVAL;
  }

  switch (val) {
  case 0:
    /* stop case */
    return khttpd_stop_server();
  case 1:
    /* start case */
    return khttpd_start_server();
  default:
    /* all other is invalid */
    return -EINVAL;
  }
}

static int try_process_port(char *data)
{
  int ret;
  long val;

  if (!data) {
    mod_print(KERN_WARNING, "try_process_port: Received NULL pointer\n");
  }

  if (*data == '\0')
    return -EINVAL;

  ret = kstrtol(data, 10, &val);
  if (ret < 0 || val < 0 || val > 65535) {
    return -EINVAL;
  }

  if (!khttpd_server_set_port(val)) {
    return -EINVAL;
  }

  return 0;
}

/* currently not used for any actual work */
static int try_process_wwwroot(char *data)
{
  if (!data) {
    mod_print(KERN_WARNING, "try_process_wwwroot: Received NULL pointer\n");
    return -EINVAL;
  }

  /* Ignore if given empty string */
  if (*data == '\0')
    return -EINVAL;

  if (!khttpd_server_set_wwwroot(data)) {
    return -EINVAL;
  }

  return 0;
}

static int try_process_maxcon(char *data)
{
  int ret;
  long val;

  if (!data) {
    mod_print(KERN_WARNING, "try_process_maxcon: Received NULL pointer\n");
    return -EINVAL;
  }

  ret = kstrtol(data, 10, &val);
  if (ret < 0 || val < 0) {
    return -EINVAL;
  }

  if (val > 10000)
    val = 10000;

  if (!khttpd_server_set_maxcon(val)) {
    return -EINVAL;
  }

  return 0;
}

static int try_process_threads(char *data)
{
  int ret;
  long val;

  if (!data) {
    mod_print(KERN_WARNING, "try_process_threads: Received NULL pointer\n");
    return -EINVAL;
  }

  ret = kstrtol(data, 10, &val);
  if (ret < 0 || val < 0) {
    return -EINVAL;
  }

  if (!khttpd_server_set_nr_threads(val)) {
    return -EINVAL;
  }

  return 0;
}

int khttpd_open(struct inode *inode, struct file *filp)
{
  char *data, *buf;

  data = PDE_DATA(inode);
  if (!data) {
    mod_print(KERN_WARNING, "khttpd_open: I was called on something I didn't create!\n");
    return -ENXIO;
  }

  switch (PTR_ERR(data)) {
    /* KHTTPD_{CONTROL,PORT,MAXCON,NRCONS,VISITORS,THREADS} use same size buffer. KHTTPD_WWWROOT uses a larger buffer */
  case _KHTTPD_CONTROL:
  case _KHTTPD_PORT:
  case _KHTTPD_WWWROOT:
  case _KHTTPD_MAXCON:
  case _KHTTPD_NRCONS:
  case _KHTTPD_VISITORS:
  case _KHTTPD_THREADS:
    if (PTR_ERR(data) == _KHTTPD_WWWROOT)
      buf = kzalloc(MAX_BUF_SIZE, GFP_KERNEL);
    else
      buf = kzalloc(MAX_SMLBUF_SIZE, GFP_KERNEL);

    if (!buf)
      return -ENOMEM;

    /* what if private_data is already in use? Maybe ProcFS uses it */
    if (filp->private_data) {
      mod_print(KERN_ERR, "filp->private_data already has something!\n");
      kfree(buf);
      return -ENOMEM;
    }

    filp->private_data = buf;
    return 0;
  default:
    mod_print(KERN_WARNING, "khttpd_open: I was called on something I didn't create! (ID: %ld)\n", PTR_ERR(data));
    return -ENXIO;
  }
}

/* when file is close, try to read instruction (if any) */
int khttpd_close(struct inode *inode, struct file *filp)
{
  char *data;
  int ret;

  ret = 0;
  data = PDE_DATA(inode);

  if (!data) {
    mod_print(KERN_WARNING, "khttpd_close: I was called on something I didn't create!\n");
    ret = -EIO;
    goto _skip_switch;
  }

  switch (PTR_ERR(data)) {
  case _KHTTPD_CONTROL:
    /* if file was opened for write */
    if (filp->f_mode & FMODE_WRITE)
      ret = try_process_status(filp->private_data);
    break;
  case _KHTTPD_PORT:
    if (filp->f_mode & FMODE_WRITE)
      ret = try_process_port(filp->private_data);
    break;
  case _KHTTPD_WWWROOT:
    if (filp->f_mode & FMODE_WRITE)
      ret = try_process_wwwroot(filp->private_data);
    break;
  case _KHTTPD_MAXCON:
    if (filp->f_mode & FMODE_WRITE)
      ret = try_process_maxcon(filp->private_data);
    break;
  case _KHTTPD_THREADS:
    if (filp->f_mode & FMODE_WRITE)
      ret = try_process_threads(filp->private_data);
    break;
  case _KHTTPD_NRCONS:
  case _KHTTPD_VISITORS:
    break;
  default:
    ret = -EIO;
    mod_print(KERN_WARNING, "khttpd_close: I was called to close something I can't create!\n");
    break;
  }

 _skip_switch:
  if (filp->private_data)
    kfree(filp->private_data);

  return ret;
}

ssize_t khttpd_write(struct file *filp, const char __user *ubuf, size_t count, loff_t *off)
{
  char *data;

  data = PDE_DATA(file_inode(filp));
  if (!data) {
    mod_print(KERN_WARNING, "khttpd_write: I was called on something I didn't create!\n");
    return -EIO;
  }

  if (!filp->private_data) {
    mod_print(KERN_ERR, "khttpd_write: filp->private_data is NULL\n");
    return -EIO;
  }

  /* we don't do error detection on count 0 */
  if (!count) return 0;

  switch (PTR_ERR(data)) {
    size_t tocopy;
  case _KHTTPD_CONTROL:
  case _KHTTPD_PORT:
  case _KHTTPD_WWWROOT:
  case _KHTTPD_MAXCON:
  case _KHTTPD_THREADS:
    /* if we are at end of file (or past it) */
    if (((*off >= (MAX_SMLBUF_SIZE - 1)) && PTR_ERR(data) != _KHTTPD_WWWROOT) || \
	(*off >= (MAX_BUF_SIZE - 1)))
      return -ENOSPC;		/* Is this better then EBIG? */

    /* a user can control offset with lseek. It is offset in 'file' */
    if (PTR_ERR(data) == _KHTTPD_WWWROOT)
      tocopy = min(count, (size_t) ((MAX_BUF_SIZE - 1) - *off));
    else
      tocopy = min(count, (size_t) ((MAX_SMLBUF_SIZE - 1) - *off));

    if (copy_from_user(filp->private_data+*off, ubuf, tocopy))
      return -EFAULT;

    *off += tocopy;
    return tocopy;
  case _KHTTPD_NRCONS:
  case _KHTTPD_VISITORS:
    return -EINVAL;
  default:
    mod_print(KERN_WARNING, "khttpd_write: I was called on something I can't write to!\n");
    return -EIO;
  }
}

ssize_t khttpd_read(struct file *filp, char __user *ubuf, size_t count, loff_t *off)
{
  char *data;

  data = PDE_DATA(file_inode(filp));
  if (!data) {
    mod_print(KERN_WARNING, "khttpd_read: I was called on something I didn't create!\n");
    return -EIO;
  }

  if (!filp->private_data) {
    mod_print(KERN_ERR, "khttpd_read: filp->private_data is NULL\n");
    return -EIO;
  }

  switch (PTR_ERR(data)) {
    size_t tocopy;
    int size;
  case _KHTTPD_CONTROL:
  case _KHTTPD_PORT:
  case _KHTTPD_WWWROOT:
  case _KHTTPD_MAXCON:
  case _KHTTPD_NRCONS:
  case _KHTTPD_VISITORS:
  case _KHTTPD_THREADS:

    /* If we are at the start OR we detect that no processing was done before and will read data */
    if (!*off || (count && *(char *)filp->private_data == '\0')) {
      switch (PTR_ERR(data)) {
      case _KHTTPD_CONTROL:
	size = snprintf(filp->private_data, MAX_SMLBUF_SIZE, "%d\n", khttpd_server_alive());
	if (size < 0)
	  return -ENOMEM;
	break;
      case _KHTTPD_PORT:
	size = snprintf(filp->private_data, MAX_SMLBUF_SIZE, "%d\n", khttpd_server_get_port());
	if (size < 0)
	  return -ENOMEM;
	break;
      case _KHTTPD_WWWROOT:
	/* khttpd_server_get_wwwroot returns a string allocated by kmalloc and MUST be kfree(d) */
	{
	  char *wwwroot = khttpd_server_get_wwwroot();
	  if (!wwwroot) {
	    mod_print(KERN_ERR, "khttpd_read: khttpd_server_get_wwwroot returned NULL\n");
	    return -ENOMEM;
	  }
	  size = snprintf(filp->private_data, MAX_BUF_SIZE, "%s\n", khttpd_server_get_wwwroot());

	  kfree(wwwroot);

	  if (size < 0)
	    return -ENOMEM;
	  break;
	}
      case _KHTTPD_MAXCON:
	size = snprintf(filp->private_data, MAX_SMLBUF_SIZE, "%d\n", khttpd_server_get_maxcon());
	if (size < 0)
	  return -ENOMEM;
	break;
      case _KHTTPD_NRCONS:
	size = snprintf(filp->private_data, MAX_SMLBUF_SIZE, "%d\n", khttpd_server_get_nrcons());
	if (size < 0)
	  return -ENOMEM;
	break;
      case _KHTTPD_VISITORS:
	size = snprintf(filp->private_data, MAX_SMLBUF_SIZE, "%lu\n", khttpd_server_get_total_visitors());
	if (size < 0)
	  return -ENOMEM;
	break;
      case _KHTTPD_THREADS:
	size = snprintf(filp->private_data, MAX_SMLBUF_SIZE, "%d\n", khttpd_server_get_nr_threads());
	if (size < 0)
	  return -ENOMEM;
	break;
      default:
	return -EIO;
      }
    } else {
      size = strlen(filp->private_data);
      /* oops */
      if (!size) {
	mod_print(KERN_WARNING, "khttpd_read: I got a string size of 0\n");
	return -ENOMEM;
      }
    }

    /* man read(2) says we *may* check for errors when count == 0, but may not. We don't */
    if (!count || *off >= size) return 0;

    /* count cannot be zero, nor can *off == size */
    tocopy = min(count, (size_t) (size-*off));
    if (copy_to_user(ubuf, filp->private_data+*off, tocopy))
      return -EFAULT;

    *off += tocopy;
    return tocopy;
  default:
    mod_print(KERN_WARNING, "khttpd_read: I was called on something I can't read from!\n");
    return -EIO;
  }
}
