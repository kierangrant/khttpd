What is kHTTPd
==============

kHTTPd was a module for the Linux Kernel from versions 2.2 to 2.4 that provided for a kernel-mode HTTP accelerator daemon.

It was designed to server static files directly from the kernel, and pass requests it could not handle to a user-space server.

For the original, see: http://www.fenrus.demon.nl/

This version is inspired by the original, but only lightly uses the original code.
See file copyrights to see which file used original code or not. Original code has been heavily modified if directly used.

Limitations
===========

- Doesn't process HTTP
- Doesn't serve files, just a simple dynamic message (for now)
- Probably not stable and safe

How to Build
============

In the Kernel
-------------

1. copy/clone to drivers/misc/khttpd
2. add `source "drivers/misc/khttpd/Kconfig"` to driver/misc/Kconfig
3. add `obj-$(CONFIG_KHTTPD)		+= khttpd/` to driver/misc/Makefile
4. Run configuration and select KHTTPD under Drivers->Miscellaneous

As a module
-----------

run `make CONFIG_KHTTPD=m` in source directory. You must pass any configuration items as arguments to make.
