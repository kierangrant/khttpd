/*
 *  control.c - Linux kernel module for kHTTPd Daemon
 *
 *  Copyright (C) 1999 Arjan van de Ven
 *  Copyright (C) 2018 Kieran Grant <kierangrant@linux.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kthread.h>
#include <linux/net.h>
#include <linux/nsproxy.h>
#include <net/sock.h>
#include <uapi/linux/in.h>
#include <linux/delay.h>
#include <linux/mutex.h>
#include "khttpd.h"

/* Server thread */
static struct task_struct *khttpd_thread;
static int status = 0;
static int inhibit_server = 0;
static unsigned long total_visitors = 0;
static int port = CONFIG_KHTTPD_DEFAULT_PORT;
char wwwroot[MAX_BUF_SIZE];
static int max_connections = CONFIG_KHTTPD_DEFAULT_MAXCONN;
static int number_of_workers = CONFIG_KHTTPD_DEFAULT_THREADS;
static struct task_struct **khttpd_workers;

static struct khttpd_request *khttpd_connection_pool;
static struct khttpd_request *khttpd_connection_pool_tail;
static int khttpd_connections;

static DEFINE_MUTEX(khttpd_control_lock);
static DEFINE_MUTEX(khttpd_pool_lock);

struct socket *create_main_socket(void)
{
  struct socket *main_socket;
  int error;
  struct sockaddr_in *sin;

  sin = kzalloc(sizeof(struct sockaddr_in), GFP_KERNEL);
  if (!sin) {
    mod_print(KERN_WARNING, "create_main_socket: Failed to allocate sockaddr_in\n");
    status = 0;
    return NULL;
  }

  /* Make socket. This is like socket(2) */
  error = sock_create_kern(current->nsproxy->net_ns, AF_INET, SOCK_STREAM, 0,
			   &main_socket);

  if (error < 0) {
    mod_print(KERN_ERR, "create_main_socket: Failed to create main socket\n");
    kfree(sin);
    status = 0;
    return NULL;
  }

  memset(sin, 0, sizeof(struct sockaddr_in));
  sin->sin_family	= AF_INET;
  sin->sin_port		= htons((unsigned short) port);
  sin->sin_addr.s_addr	= INADDR_ANY;

  /* Bind to socket. This is like bind(2) */
  error = kernel_bind(main_socket, (struct sockaddr *) sin,
		      sizeof(struct sockaddr_in));
  if (error < 0) {
    mod_print(KERN_ERR, "khttp_server: Failed to bind socket\n");
    sock_release(main_socket);
    kfree(sin);
    status = 0;
    return NULL;
  }

  /* Now start to listen. Like listen(2) */
  error = kernel_listen(main_socket, SOMAXCONN);
  if (error < 0) {
    mod_print(KERN_ERR, "create_main_socket: Failed to listen to socket\n");
    sock_release(main_socket);
    kfree(sin);
    status = 0;
    return NULL;
  }

  kfree(sin);
  return main_socket;
}

/* main server thread. For now there will be just one thread doing everything */
int khttpd_server(void *useless)
{
  int error, i, j;
  struct socket *main_socket = create_main_socket();
  struct khttpd_request *req, *temp;

  if (!main_socket)
    return -1;

  khttpd_workers = kmalloc_array(number_of_workers, sizeof(struct khttpd_thread*), GFP_KERNEL);
  if (!khttpd_workers) {
    mod_print(KERN_WARNING, "khttpd_server: Failed to allocate worker thread array\n");
    sock_release(main_socket);
    main_socket = NULL;
    return -1;
  }

  /* NULL != 0 in ISO C90 */
  for (i = 0; i < number_of_workers; i++) khttpd_workers[i] = NULL;

  for (i = 0; i < number_of_workers; i++) {
    khttpd_workers[i] = kthread_create(khttpd_worker, NULL, "kHTTPd worker/%d", i);
    if (khttpd_workers[i])
      continue;
    /* If an allocation failed, destroy all threads */
    for (j = 0; j < i; j++) kthread_stop(khttpd_workers[j]);
    sock_release(main_socket);
    return -1;
  }

  mutex_lock(&khttpd_pool_lock);
  khttpd_connections = 0;
  khttpd_connection_pool = NULL;
  khttpd_connection_pool_tail = NULL;
  total_visitors = 0;
  mutex_unlock(&khttpd_pool_lock);

  for (i = 0; i < number_of_workers; i++) wake_up_process(khttpd_workers[i]);

  /* while not told to terminate */
  while (!kthread_should_stop()) {
    struct msghdr *msg;
    struct kvec *iovec;
    struct socket *con;
    int control_sleep = 1;

    msg = kzalloc(sizeof(struct msghdr), GFP_KERNEL);
    if (!msg)
      goto _nofree;

    iovec = kzalloc(sizeof(struct kvec), GFP_KERNEL);
    if (!iovec)
      goto _free_msg;

    /* Begin Locked Area */
    mutex_lock(&khttpd_pool_lock);

    if (khttpd_connections >= max_connections)
      goto _free_iovec;

    error = kernel_accept(main_socket, &con, SOCK_NONBLOCK);
    if (error < 0)
      goto _free_iovec;

    req = khttpd_request_new(con);
    if (!req) {
      kernel_sock_shutdown(con, SHUT_RDWR);
      sock_release(con);
      goto _free_iovec;
    }

    khttpd_connections++;

    /* If there is a tail there is a head too.
       Tail always point to new, head only if there was no existing item */
    if (khttpd_connection_pool_tail) {
      khttpd_connection_pool_tail->next = req;
    } else {
      khttpd_connection_pool = req;
    }

    khttpd_connection_pool_tail = req;
    control_sleep = 0;

  _free_iovec:
    mutex_unlock(&khttpd_pool_lock);
    /* End Locked Area */
    kfree(iovec);
  _free_msg:
    kfree(msg);
  _nofree:
    if (control_sleep)
      usleep_range(1000,10000);
  }

  /* First stop all worker threads */
  for (i = 0; i < number_of_workers; i ++)
    kthread_stop(khttpd_workers[i]);

  sock_release(main_socket);

  for (req = khttpd_connection_pool; req;) {
    temp = req->next;
    khttpd_request_release(req);
    req = temp;
  }

  status = 0;
  return 0;
}

/* start's the server */
int khttpd_start_server(void)
{
  int ret;

  mutex_lock(&khttpd_control_lock);

  if (status || inhibit_server) {
    ret = 0;
    goto _end;
  }

  khttpd_thread = kthread_run(khttpd_server, NULL, "kHTTPd control");
  if (IS_ERR(khttpd_thread)) {
    mod_print(KERN_ERR, "Failed to create server\n");
    ret = PTR_ERR(khttpd_thread);
    goto _end;
  }

  try_module_get(THIS_MODULE);
  status = 1;
  ret = 0;
 _end:

  mutex_unlock(&khttpd_control_lock);

  return ret;
}

/* Inhibit server from starting */
void khttpd_inhibit_server(void)
{
  mutex_lock(&khttpd_control_lock);
  inhibit_server = 1;
  mutex_unlock(&khttpd_control_lock);
}

/* stop's the server. This function will BLOCK. */
int khttpd_stop_server(void)
{
  int ret;

  mutex_lock(&khttpd_control_lock);

  if (!status) {
    ret = 0;
    goto _end;
  }

  if (kthread_stop(khttpd_thread) < 0)
    mod_print(KERN_ERR, "khttpd_stop_server: Main thread errored out\n");

  status = 0;			/* just in case */
  module_put(THIS_MODULE);

 _end:

  mutex_unlock(&khttpd_control_lock);

  return ret;
}

/* simple accessor */
int khttpd_server_alive(void)
{
  int ret;

  mutex_lock(&khttpd_control_lock);
  ret = status;
  mutex_unlock(&khttpd_control_lock);

  return ret;
}

int khttpd_server_get_port(void)
{
  int ret;
  mutex_lock(&khttpd_control_lock);
  ret = port;
  mutex_unlock(&khttpd_control_lock);

  return port;
}

/* cannot set whilst server is running */
int khttpd_server_set_port(int newport)
{
  int ret;

  mutex_lock(&khttpd_control_lock);
  if (status) {
    ret = 0;
    goto _end;
  }

  port = newport;
  ret = 1;

 _end:
  mutex_unlock(&khttpd_control_lock);

  return ret;
}

char *khttpd_server_get_wwwroot(void)
{
  size_t len;
  char *result;

  mutex_lock(&khttpd_control_lock);
  result = kzalloc(MAX_BUF_SIZE, GFP_KERNEL);

  if (!result)
    goto _end;

  len = strnlen(wwwroot, MAX_BUF_SIZE);

  if (len == MAX_BUF_SIZE) {
    mod_print(KERN_WARNING, "khttpd_server_get_wwwroot: wwwroot is missing NULL terminator!\n");
    len--;
  }

  (void)strncpy(result, wwwroot, len+1);

 _end:
  mutex_unlock(&khttpd_control_lock);

  return result;
}

/* cannot set whilst server is running */
int khttpd_server_set_wwwroot(char *root)
{
  size_t len;
  int ret;

  mutex_lock(&khttpd_control_lock);
  if (!root) {
    mod_print(KERN_WARNING, "httpd_server_set_wwwroot: Recevied NULL pointer\n");
    ret = 0;
    goto _end;
  }

  if (status) {
    ret = 0;
    goto _end;
  }

  len = strnlen(root, MAX_BUF_SIZE);
  if (len >= MAX_BUF_SIZE) {
    ret = 0;
    goto _end;
  }

  (void) strncpy(wwwroot, root, len+1);
  ret = 1;

 _end:
  mutex_unlock(&khttpd_control_lock);

  return ret;
}

int khttpd_server_get_nrcons(void)
{
  return khttpd_connections;
}

int khttpd_server_get_maxcon(void)
{
  return max_connections;
}

int khttpd_server_set_maxcon(int newmax)
{
  int ret;

  mutex_lock(&khttpd_control_lock);

  if (status) {
    ret = 0;
    goto _end;
  }

  max_connections = newmax;
  ret = 1;

 _end:
  mutex_unlock(&khttpd_control_lock);

  return ret;
}

unsigned long khttpd_server_get_total_visitors(void)
{
  /* we don't care if we are inaccurate */
  return total_visitors;
}

int khttpd_server_get_nr_threads(void)
{
  int ret;
  mutex_lock(&khttpd_control_lock);
  ret = number_of_workers;
  mutex_unlock(&khttpd_control_lock);

  return ret;
}

int khttpd_server_set_nr_threads(int nr_threads)
{
  int ret;

  mutex_lock(&khttpd_control_lock);

  if (status) {
    ret = 0;
  } else {
    ret = 1;
    if (nr_threads < 0)
      nr_threads = 1;
    number_of_workers = nr_threads;
  }

  mutex_unlock(&khttpd_control_lock);

  return ret;
}

struct khttpd_request *khttpd_request_get(void)
{
  struct khttpd_request *req;

  mutex_lock(&khttpd_pool_lock);

  if (khttpd_connection_pool && khttpd_connections <= 0) {
    mod_print(KERN_ERR, "khttpd_get_request: Inconsistency detected in connection count!\n");
    khttpd_connections = 1;
  }

  if (!khttpd_connection_pool) {
    req = NULL;
  } else {
    req = khttpd_connection_pool;
    khttpd_connection_pool = req->next;
    if (!req->next)
      khttpd_connection_pool_tail = NULL;
    req->visitor_number = ++total_visitors;
  }

  mutex_unlock(&khttpd_pool_lock);
  return req;
}

struct khttpd_request* khttpd_request_new(struct socket *sock)
{
  struct khttpd_request *req;

  req = kzalloc(sizeof(struct khttpd_request), GFP_KERNEL);
  if (!req) {
    mod_print(KERN_WARNING, "khttpd_request_new: Failed to allocate khttpd_request struct\n");
    return NULL;
  }

  req->next = NULL;
  req->sock = sock;

  return req;
}

void khttpd_request_release(struct khttpd_request *request)
{
  if (!request) {
    mod_print(KERN_WARNING, "khttpd_request_release: Called with NULL pointer\n");
    return;
  }

  kernel_sock_shutdown(request->sock, SHUT_RDWR);
  sock_release(request->sock);
  kfree(request);

  mutex_lock(&khttpd_pool_lock);
  khttpd_connections--;
  mutex_unlock(&khttpd_pool_lock);

  return;
}
