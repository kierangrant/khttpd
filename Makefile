khttpd-objs = init.o procfs.o control.o worker.o
obj-$(CONFIG_KHTTPD) += khttpd.o

all:
	$(MAKE) -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	$(MAKE) -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
